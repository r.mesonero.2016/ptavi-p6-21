#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import os
import simplertp
import random

# Puerto
PORT = 6001
audio_file = sys.argv[3]


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    UDP echo handler class
    """
    dicc = {}

    def handle(self):
        string = ""
        # Recoge los datos del cliente:
        for line in self.rfile:
            string = string + line.decode('utf-8')
        # Se ordenan los datos:
        ip = self.client_address[0]
        puerto = self.client_address[1]
        lista = string.split("\r\n")
        peticion = lista[0]
        lista_peticion = peticion.split(" ")
        registro_nombre = lista_peticion[1].strip('sip:')
        usuario = registro_nombre.split("@")

        # Respuestas del servidor:
        if lista_peticion[0] == "INVITE":
            if usuario[1][0] != "1" or usuario[0][0] == "1":
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
            else:
                descripcion_sdp = lista[1:10]
                descripcion_sdp_correcta = []
                for string in descripcion_sdp:
                    if string != "":
                        descripcion_sdp_correcta.append(string)
                content_type = descripcion_sdp_correcta[0]
                content_length = descripcion_sdp_correcta[1].split(" ")
                cabecera_longitud = content_length[0] + " "
                version = descripcion_sdp_correcta[2]
                info_usuario_sdp = descripcion_sdp_correcta[3]
                info_sdp = info_usuario_sdp.split(" ")
                usuario_sdp = info_sdp[0][2:]
                ip_sdp = info_sdp[1]
                self.dicc["usuario"] = usuario_sdp
                self.dicc["ip"] = ip_sdp
                session = descripcion_sdp_correcta[4]
                tiempo = descripcion_sdp_correcta[5]
                info_multimedia_sdp = descripcion_sdp_correcta[6]
                info_multimedia = info_multimedia_sdp.split(" ")
                puerto_rtp_usuario = info_multimedia[1]
                self.dicc["puerto"] = puerto_rtp_usuario
                puerto_rtp_server = '27138'
                info_multimedia[1] = puerto_rtp_server
                enviar_multimedia = " ".join(info_multimedia)
                size_cuerpo = sys.getsizeof(version + info_usuario_sdp + session + tiempo + enviar_multimedia)
                self.wfile.write(b"SIP/2.0 200 OK\r\n" +
                                 bytes(content_type + '\r\n', 'utf-8') +
                                 bytes(cabecera_longitud + str(size_cuerpo) + '\r\n\r\n', 'utf-8') +
                                 bytes(version + '\r\n', 'utf-8') +
                                 bytes(info_usuario_sdp + '\r\n', 'utf-8') +
                                 bytes(session + '\r\n', 'utf-8') +
                                 bytes(tiempo + '\r\n', 'utf-8') +
                                 bytes(enviar_multimedia + '\r\n', 'utf-8'))
        elif lista_peticion[0] != "INVITE" and lista_peticion[0] != "BYE" and lista_peticion[0] != "ACK":
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
        elif usuario[1][0] != "1" or usuario[0][0] == "1":
            self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
        elif lista_peticion[2] != "SIP/2.0":
            self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
        elif len(lista_peticion) != 3:
            self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
        elif lista_peticion[0] == "BYE":
            self.wfile.write(b"SIP/2.0 200 OK\r\n")
        elif lista_peticion[0] == "ACK":
            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(pad_flag=0, ext_flag=0, cc=0, ssrc=random.randrange(10))
            audio = simplertp.RtpPayloadMp3(audio_file)
            simplertp.send_rtp_packet(RTP_header, audio, self.dicc["ip"], int(self.dicc["puerto"]))


def main():
    # Creamos servidor de eco y escuchamos
    # Listens at port PORT (my address)
    # and calls the EchoHandler class to manage the request
    try:
        ip = sys.argv[1]
        port = sys.argv[2]
        audio_file = sys.argv[3]
        if os.path.isfile("/home/alumnos/rmesoner/Escritorio/PTAVI2/ptavi-p6-21/cancion.mp3"):
            pass
        else:
            print("No existe el fichero")
            exit()
    except IndexError:
        sys.exit("Usage: python3 server.py <IP> <port> <audio_file>")

    try:
        serv = socketserver.UDPServer(('', PORT), EchoHandler)
        print("Listening...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
