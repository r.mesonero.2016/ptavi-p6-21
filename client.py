#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys

# Dirección IP y puerto del servidor.
SERVER = 'localhost'
PORT = 6001


def main():
    # Definimos los argumentos que se utilizan para ejecutar el cliente
    try:
        method = sys.argv[1]
        user = sys.argv[2]
        list_datos = user.split('@')
        list_num = list_datos[1].split(':')
        reciever = list_datos[0]
        ip = list_num[0]
        sip_port = list_num[1]
    except IndexError:
        sys.exit("Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>")

    # Creamos el socket y lo configuramos
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((SERVER, PORT))
            # Enviamos al servidor la informacion SDP:
            if method.upper() == "INVITE":
                cabecera_content_type = 'Content-Type: application/sdp'
                cabecera_longitud = 'Content-Length: '
                version = "0"
                origen = reciever + "@" + reciever + " " + ip
                nombre_sesion = "misesion"
                tiempo = "0"
                rtp_port = "7080"
                multimedia = "audio " + rtp_port + " RTP"
                size_cuerpo = sys.getsizeof("v=" + version + '\r\n' + "o=" + origen + '\r\n' +
                                            "s=" + nombre_sesion + '\r\n' + "t=" + tiempo + '\r\n' +
                                            "m=" + multimedia + '\r\n')

                print(method.upper() + " sip:" + reciever + '@' + ip + " SIP/2.0")
                print(cabecera_content_type)
                print(cabecera_longitud + str(size_cuerpo))
                print()
                print("v=" + version)
                print("o=" + origen)
                print("s=" + nombre_sesion)
                print("t=" + tiempo)
                print("m=" + multimedia)
                print()
                my_socket.send(bytes(method.upper() + ' sip:' + reciever + '@' + ip + ' SIP/2.0' + '\r\n', 'utf-8') +
                               bytes(cabecera_content_type + '\r\n', 'utf-8') +
                               bytes(cabecera_longitud + str(size_cuerpo) + '\r\n', 'utf-8') + b'\r\n\r\n' +
                               bytes('v=' + version + '\r\n', 'utf-8') +
                               bytes('o=' + origen + '\r\n', 'utf-8') +
                               bytes('s=' + nombre_sesion + '\r\n', 'utf-8') +
                               bytes('t=' + tiempo + '\r\n', 'utf-8') +
                               bytes('m=' + multimedia + '\r\n', 'utf-8') + b'\r\n\r\n')
            else:
                print(method.upper() + " sip:" + reciever + '@' + ip + " SIP/2.0")
                print()
                my_socket.send(bytes(method.upper() + ' sip:' + reciever + '@' + ip + ' SIP/2.0' + '\r\n\r\n', 'utf-8'))
            # Recibimos del servidor
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))

        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
